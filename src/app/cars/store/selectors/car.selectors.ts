import {createSelector} from "@ngrx/store";
import * as fromCarsReducers from "../reducers/cars.reducers"
import {CarsState, getCarsState} from "../reducers";
import * as fromRootStore from "../../../store";

export const getCarStateSelector = createSelector(getCarsState, (state: CarsState) => state.cars);

export const getAllCarsEntitiesSelector = createSelector(getCarStateSelector, (fromCarsReducers.getCarsEntities));
export const getAllCarsSelector = createSelector(getAllCarsEntitiesSelector, (entities) => {

  return Object.keys(entities).map((id: string) => {
    return entities[parseInt(id, 10)];
  })
});

export const getSelectedCarSelector = createSelector(getAllCarsEntitiesSelector, fromRootStore.getRouterState, (entities, route) => {
  return route.state && entities[route.state.params.id];
});

export const getCarsLodedSelector = createSelector(getCarStateSelector, (fromCarsReducers.getCarsLoaded));
export const getCarsLodingSelector = createSelector(getCarStateSelector, (fromCarsReducers.getCarsLoading));
