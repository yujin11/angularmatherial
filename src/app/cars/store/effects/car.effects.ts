import {Injectable} from "@angular/core";
import {Actions, Effect, ofType} from "@ngrx/effects";
import {CarService} from "../../services/car.service";
import * as carActions from "../actions";
import {map, switchMap} from "rxjs/operators";

@Injectable()
export class CarEffects {

  constructor(protected action: Actions, protected carService: CarService){

  }

  @Effect()
  loadCars$ = this.action.pipe(
    ofType(carActions.LoadCars),
    switchMap(()=>this.carService.getCarsList()),
    map(cars=> new carActions.LoadCarsSuccessAction(cars))
  )

}
