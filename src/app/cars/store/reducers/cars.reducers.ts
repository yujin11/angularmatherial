import {CarModel} from "../../shared/car.model";
import * as fromActions from "../actions";

export interface CarState {
  entities: { [id: number]: CarModel },
  loaded: boolean,
  loading: boolean
}

export const initialState: CarState = {
  entities: {},
  loaded: false,
  loading: false
}

export function reducer(state = initialState, action: fromActions.carActions): CarState {
  switch (action.type) {
    case fromActions.LoadCars:
      return {
        ...state,
        loading: true
      }
    case fromActions.LoadCarsSuccess:
      const entities = action.payload.reduce((entities: { [id: number]: CarModel }, car: CarModel) => {
        return {
          ...entities,
          [car.id]: car
        }
      }, {
        ...state.entities
      });
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      };
    case fromActions.LoadCarsFail:
      return {
        ...state,
        loaded: false,
        loading: false
      }
  }
  return state;
}

export const getCarsEntities = (state: CarState) => state.entities;
export const getCarsLoaded = (state: CarState) => state.loaded;
export const getCarsLoading = (state: CarState) => state.loading;
