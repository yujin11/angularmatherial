import {ActionReducerMap, createFeatureSelector} from "@ngrx/store";

import * as fromReducers from "./cars.reducers";

export interface CarsState {
  cars:fromReducers.CarState
}

export const carsReducers: ActionReducerMap<CarsState> = {
  cars: fromReducers.reducer
}

export const getCarsState = createFeatureSelector('cars');
