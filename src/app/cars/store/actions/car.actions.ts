import {Action} from "@ngrx/store";
import {CarModel} from "../../shared/car.model";

export const LoadCars = "[Cars] Load";
export const LoadCarsSuccess = "[Cars] Load Success";
export const LoadCarsFail = "[Cars] - Load Fail";

export class LoadCarsAction implements Action{
  readonly type: string = LoadCars;
  payload: CarModel[];
}
export class LoadCarsSuccessAction implements Action{
  readonly type: string = LoadCarsSuccess;
  constructor(public payload: CarModel[]){}
}
export class LoadCarsFailAction implements Action{
  readonly type: string = LoadCarsFail;
  constructor(public payload: CarModel[]){}
}

export type carActions = LoadCarsAction | LoadCarsSuccessAction | LoadCarsFailAction;
