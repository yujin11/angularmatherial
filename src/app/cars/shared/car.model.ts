import {Data} from "@angular/router";

export interface CarModel {
  id: number;
  bodyType: string;
  make: string;
  model: string;
  year: Data;
  image: string;
}
