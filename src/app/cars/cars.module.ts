import {NgModule} from "@angular/core";
import {carsComponents} from "./components";
import {CarService} from "./services/car.service";
import {BrowserModule} from "@angular/platform-browser";
import {RouterModule} from "@angular/router";
import {carsRoutes} from "./routes/cars.routes";
import {HttpClientModule} from "@angular/common/http";
import {StoreModule} from "@ngrx/store";
import {EffectsModule} from "@ngrx/effects";
import {carEffects} from "./store/effects";
import {CarRouterGuards} from "./routes/car.router.guards";
import {carsReducers} from "./store/reducers";
import {MatButtonModule, MatCardModule} from "@angular/material";

@NgModule({
  imports:[
    BrowserModule,
    HttpClientModule,
    RouterModule.forChild(carsRoutes),
    StoreModule.forFeature('cars', carsReducers),
    EffectsModule.forFeature(carEffects),
    MatCardModule,
    MatButtonModule
  ],
  declarations:carsComponents,
  providers: [
    CarService,
    CarRouterGuards
  ]
})
export class CarsModule {

}
