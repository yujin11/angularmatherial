import {Inject, Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import * as fromStore from "../store";
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from "@angular/router";
import {Observable, of} from "rxjs";
import {catchError, filter, switchMap, take, tap} from "rxjs/operators";

@Injectable()
export class CarRouterGuards implements CanActivate {
  constructor(@Inject(Store) protected store: Store<fromStore.CarsState>) {

  }

  canActivate(): Observable<boolean> {
    return this.checkCarData().pipe(
      switchMap(()=> of(true)),
      catchError(()=> of(false))
    )
  }

  checkCarData() {
    return this.store.select(fromStore.getCarsLodedSelector).pipe(
      tap(loaded => {
        if (!loaded)
          this.store.dispatch(new fromStore.LoadCarsAction())
      }),
      filter((loaded: boolean) => loaded),
      take(1)
    )
  }

}
