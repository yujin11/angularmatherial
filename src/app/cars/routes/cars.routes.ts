import {Routes} from "@angular/router";
import {CarsListComponent} from "../components/cars-list.component";
import {CarComponent} from "../components/car.component";
import {CarRouterGuards} from "./car.router.guards";

export const carsRoutes: Routes = [{
  path: "cars",
  component: CarsListComponent
},
  {
    path: "car/:id",
    component: CarComponent,
    canActivate: [CarRouterGuards]
  }]
