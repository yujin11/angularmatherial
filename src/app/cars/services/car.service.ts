import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {CarModel} from "../shared/car.model";
import {map} from "rxjs/operators";

@Injectable()
export class CarService {
  constructor(protected http: HttpClient){}

  public getCarsList():Observable<CarModel[]>{
    return this.http.get('./assets/data/cars/data.json').pipe(map(cars=>cars)) as Observable<CarModel[]>;
  }
}
