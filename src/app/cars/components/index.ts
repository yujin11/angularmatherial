import {CarsListComponent} from "./cars-list.component";
import {CarComponent} from "./car.component";

export const carsComponents = [CarsListComponent, CarComponent];
