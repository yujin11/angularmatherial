import {Component, Input, OnInit} from "@angular/core";
import {CarModel} from "../shared/car.model";
import {CarsState} from "../store/reducers"
import * as stateSelectors from "../store/selectors";
import {Store} from "@ngrx/store";
import {Observable} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: "car",
  templateUrl: "./car.component.html",
  styleUrls: ["./car.component.less"]
})
export class CarComponent implements OnInit {

  car$: Observable<CarModel>;

  constructor(protected store: Store<CarsState>, protected router: Router) {}

  onBack(){
    this.router.navigateByUrl("/cars");
  }

  ngOnInit(): void {
    this.car$ = this.store.select(stateSelectors.getSelectedCarSelector);
  }
}
