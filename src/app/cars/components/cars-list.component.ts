import {ChangeDetectionStrategy, Component} from "@angular/core";
import {Observable} from "rxjs";
import {CarModel} from "../shared/car.model";
import {Store} from "@ngrx/store";
import * as fromCars from "../store";
import {Router} from "@angular/router";

@Component({
  selector: "cars-list",
  templateUrl: "./cars-list.component.html",
  styleUrls: ["./cars-list.component.less"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CarsListComponent {
  cars$: Observable<CarModel[]>;

  constructor(store: Store<fromCars.CarsState>, protected route: Router) {
    this.cars$ = store.select(fromCars.getAllCarsSelector);
    store.dispatch(new fromCars.LoadCarsAction());
  }

  showSelectedCar(id:number){
    this.route.navigate(['/car', id]);
  }

}
