import {ShareModule} from "./app.share.module";
import {AppComponent} from './app.component';

import {AppIndexModule} from './index/app.index.module';
import {NgModule} from "@angular/core";
import {BrowserModule} from "@angular/platform-browser";
import {StoreModule} from "@ngrx/store";
import {HttpModule} from "@angular/http";
import {EffectsModule} from "@ngrx/effects";
import {StoreDevtoolsModule} from "@ngrx/store-devtools";
import {RouterModule} from "@angular/router";
import {routes} from "./routers/app.routes";
import {CustomSrializer, reducers} from "./store/reducers";
import {StoreRouterConnectingModule, RouterStateSerializer} from "@ngrx/router-store";
import {CarsModule} from "./cars/cars.module";
import {AppObserversModule} from "./observers";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppIndexModule,
    CarsModule,
    RouterModule.forRoot(routes),
    ShareModule,
    StoreModule.forRoot(reducers, {}),
    EffectsModule.forRoot([]),
    StoreDevtoolsModule.instrument(),
    HttpModule,
    StoreRouterConnectingModule,
    AppObserversModule
  ],
  providers: [
    {provide: RouterStateSerializer, useClass: CustomSrializer}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
