import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from "@angular/core";
import {AppServices} from "../../services/app.services";
import {IPerson} from "../../shared/IPerson";
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import * as fromPerson from "../store";

@Component({
  selector: 'app-person',
  templateUrl: './persons.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PersonsComponent {

  storePersons$: Observable<IPerson[]>;

  @Output('event')
  event: EventEmitter<number> = new EventEmitter<number>();

  @Input()
  number: number;

  constructor(private appService: AppServices, private store: Store<fromPerson.PersonState>) {
    this.storePersons$ = store.select<any>(fromPerson.getAllPersonsSelector);
    store.dispatch(new fromPerson.LoadPersons());
  }

  increment(){
    this.number++;
    this.event.emit(this.number);
  }

  decrement(){
    this.number--;
    this.event.emit(this.number);
  }

}
