import {Component, Input, OnInit} from "@angular/core";
import {IPerson} from "../../shared/IPerson";
import {from, Observable} from "rxjs";
import {Store} from "@ngrx/store";
import * as fromStore from "../store";
import {map} from "rxjs/operators";

@Component({
  selector: 'person',
  templateUrl: './person.component.html'
})
export class PersonComponent implements OnInit{

  public person$: Observable<IPerson>;
  public loaded$: Observable<boolean>;

  constructor(protected store: Store<fromStore.PersonState>){}

  ngOnInit(): void {
      this.person$ = this.store.select(fromStore.getSelectedPerson);
  }



}
