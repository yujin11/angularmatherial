import {NgModule} from "@angular/core";
import {StoreModule} from "@ngrx/store";
import {reducer, persronEffects} from "./store";
import {EffectsModule} from "@ngrx/effects";
import {personComponents} from "./components";
import {AppServices} from "../services/app.services";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {PersonComponent} from "./components/person.component";
import {RouterModule} from "@angular/router";
import {routes} from "./person.routes";
import {PersonGuards} from "./person.guards";
import {MatButtonModule} from "@angular/material";

@NgModule({
  declarations: [
    PersonComponent,
    personComponents
  ],
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forChild(routes),
    StoreModule.forFeature('person', reducer),
    EffectsModule.forFeature(persronEffects),
    MatButtonModule,

  ],
  providers: [
    AppServices,
    PersonGuards
  ],
  exports: [
    personComponents
  ]
})
export class PersonsModule {

}
