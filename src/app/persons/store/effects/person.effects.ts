import {Actions, Effect, ofType} from "@ngrx/effects";
import {Injectable} from "@angular/core";
import * as personActions from "../actions";
import {catchError, map, switchMap} from "rxjs/operators";
import {AppServices} from "../../../services/app.services";

@Injectable()
export class PersonEffects {

  constructor(protected actions$: Actions, protected appService: AppServices) {
  }

  @Effect()
  loadPersons$ = this.actions$.pipe(
    ofType(personActions.LOAD_Person),
    switchMap(() => this.appService.getDataFile()),
    map((persons) => {
      console.log('Persons: ', persons);
      return new personActions.LoadPersonsSuccess(persons)
    })
  );
}
