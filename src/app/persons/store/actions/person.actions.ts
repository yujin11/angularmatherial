import {Action} from "@ngrx/store";
import {IPerson} from "../../../shared/IPerson";

export const LOAD_Person = '[Person] Load';
export const LOAD_Person_success = '[Person] Load Success';
export const LOAD_Person_fail = '[Person] Load Fail';

export class LoadPersons implements Action{
  readonly type: string = LOAD_Person;
  payload: IPerson[];
}
export class LoadPersonsSuccess implements Action{
  readonly type: string = LOAD_Person_success;
  constructor(public payload:IPerson[]){}
}
export class LoadPersonsFail implements Action{
  readonly type: string = LOAD_Person_fail;
  constructor(public payload:IPerson[]){}
}
export type PersonActions = LoadPersonsFail | LoadPersonsSuccess | LoadPersons;
