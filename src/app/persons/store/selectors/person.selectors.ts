import {createSelector} from "@ngrx/store";
import * as fromPersons from "../reducers/person.reducers";
import {PersonState, getPersonsState} from "../reducers";
import * as fromRoot from "../../../store"

export const getPersonState = createSelector(getPersonsState, (state: PersonState) => {
  return state.persons;
});

export const getAllPersonsEntittiesSelector = createSelector(getPersonState, fromPersons.getPersonsEntities);
export const getAllPersonsSelector = createSelector(getAllPersonsEntittiesSelector, (entities) => {
  return Object.keys(entities).map((name: string) => {
    return entities[name];
  })
});

export const getSelectedPerson = createSelector(getAllPersonsEntittiesSelector, fromRoot.getRouterState, (entities, router) => {
  return router.state && entities[router.state.params.personId];
});
export const getPersonsLoadingSelector = createSelector(getPersonState, fromPersons.getPersonsLoading);
export const getPersonsLoadedSelector = createSelector(getPersonState, fromPersons.getPersonsLoaded);
