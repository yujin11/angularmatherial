import * as fromPersons from "./person.reducers";
import {ActionReducerMap, createFeatureSelector, createSelector} from "@ngrx/store";

export interface PersonState {
  persons: fromPersons.PersonState
}

export const reducer: ActionReducerMap<PersonState> = {
  persons: fromPersons.reducer
};

export const getPersonsState = createFeatureSelector('person');
