import {IPerson} from "../../../shared/IPerson";
import * as fromPersons from "../actions/person.actions";

export interface PersonState {
  entities: { [name: string]: IPerson };
  loading: boolean;
  loaded: boolean;
}

export const initialState: PersonState = {
  entities: {},
  loading: false,
  loaded: false
};

export function reducer(
  state = initialState,
  action: fromPersons.PersonActions
): PersonState {
  switch (action.type) {
    case fromPersons.LOAD_Person:
      return {
        ...state,
        loading: true
      };
    case fromPersons.LOAD_Person_success:
      const entities = action.payload.reduce((entities: {[name: string]: IPerson}, person:IPerson) => {
        return {
          ...entities,
          [person.name]:person
        }
      },{
        ...state.entities
      });
      return {
        ...state,
        loading: false,
        loaded: true,
        entities
      };
    case fromPersons.LOAD_Person_fail:
      return {
        ...state,
        loading: false,
        loaded: false,
        entities
      }
  }
  return state;
}

export const getPersonsEntities = (state: PersonState) => state.entities;
export const getPersonsLoading = (state: PersonState) => state.loading;
export const getPersonsLoaded = (state: PersonState) => state.loaded;
