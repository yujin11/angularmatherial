import {RouterModule, Routes} from "@angular/router";
import {PersonComponent} from "./components/person.component";
import {PersonGuards} from "./person.guards";

export const routes: Routes = [
  {
    path: 'person/:personId',
    component: PersonComponent,
    canActivate: [PersonGuards]
  }
]
