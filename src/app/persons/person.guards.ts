import {CanActivate} from "@angular/router";
import {Observable, of} from "rxjs";
import {Inject, Injectable} from "@angular/core";
import {Store} from "@ngrx/store";
import {take, switchMap, catchError, tap, filter, map} from "rxjs/operators";
import * as fromStore from "./store";

Injectable()
export class PersonGuards implements CanActivate {

  constructor(@Inject(Store) protected store: Store<fromStore.PersonState>) {
  }

  canActivate(): Observable<boolean> {
    return this.checkPersonData().pipe(
      switchMap(() => of(true)),
      catchError(() => of(false))
    )
  }

  private checkPersonData(): Observable<boolean> {
    return this.store.select<any>(fromStore.getPersonsLoadedSelector).pipe(
      tap(loaded => {
        if (!loaded)
          this.store.dispatch(new fromStore.LoadPersons())
      }),
      filter((loaded: boolean) => loaded),
      take(1)
    );
  }
}
