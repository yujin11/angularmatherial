import {FormsComponent} from "./container/forms.component";
import {formsRoutes} from "./routes";

export const components = [FormsComponent];
export {formsRoutes} from './routes';
