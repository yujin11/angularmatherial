import {Component} from "@angular/core";
import {FormControl, FormGroup, Validators} from "@angular/forms";

interface myFormInteface {
  name: string,
  address: {
    city: City,
    street: string,
    number: number
  }
}

interface City {
  id: number,
  name: string
}

@Component({
  selector: 'forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.less']
})
export class FormsComponent {
  myForm: FormGroup;
  cities: City[];
  isChecked: boolean;

  constructor() {
    this.cities = [{
      id: 0,
      name: 'Nikolaev'
    }, {
      id: 1,
      name: 'Kiev'
    }, {
      id: 2,
      name: 'Lviv'
    }]
    this.myForm = new FormGroup({
      name: new FormControl('', Validators.required),
      ich: new FormControl(this.isChecked),
      address: new FormGroup({
        city: new FormControl(this.cities[0], [Validators.required]),
        street: new FormControl(''),
        number: new FormControl()
      })
    });
  }

  onSubmit(form: FormGroup) {
    console.log(form);
  }

}
