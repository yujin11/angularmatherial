import {NgModule} from "@angular/core";
import {components, formsRoutes} from "./index";
import {CommonModule} from "@angular/common";
import {BrowserModule} from "@angular/platform-browser";
import {
  MatButtonModule,
  MatCheckboxModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule
} from "@angular/material";
import {ReactiveFormsModule} from "@angular/forms";
import {RouterModule} from "@angular/router";

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forChild(formsRoutes),
    MatButtonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatCheckboxModule
  ],
  declarations: [
    components
  ]
})
export class CustomFormsModule {

}
