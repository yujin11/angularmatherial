import {Route, Routes} from "@angular/router";
import {FormsComponent} from "./container/forms.component";

export const formsRoutes: Routes = [
  {
    path: 'forms',
    component: FormsComponent
  }
];
