import {Routes} from "@angular/router";
import {ChildComponent} from "./components/child.component";

export const childRoute: Routes = [
  {
    path: 'child',
    component: ChildComponent
  }
]
