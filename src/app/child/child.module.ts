import {NgModule} from "@angular/core";
import {childRoute} from "./index";
import {ChildComponent} from "./components/child.component";
import {RouterModule} from "@angular/router";
import {BrowserModule} from "@angular/platform-browser";
import {FormsModule} from "@angular/forms";

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forChild(childRoute)
  ],
  declarations: [
    ChildComponent
  ],
  exports:[
    ChildComponent,
    RouterModule
  ]
})
export class ChildModule {

}
