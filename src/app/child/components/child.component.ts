import {ChangeDetectorRef, Component, Input} from "@angular/core";

@Component({
  selector: "app-child",
  templateUrl: "./child.component.html"
})
export class ChildComponent {
  public info: string;

  @Input("info") set setInfo(info: string) {
    this.info = info;
  }

  constructor(protected cd: ChangeDetectorRef) {

  }
}
