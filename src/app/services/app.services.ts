import {Http} from '@angular/http';
import {Injectable} from '@angular/core';
import {observable, Observable, Observer} from 'rxjs';
import {IPerson} from '../shared/IPerson';
import {map} from 'rxjs/operators';

@Injectable()
export class AppServices {
  constructor(private http: Http) {
  }

  getDataFile(): Observable<any> {
    return Observable.create((observer: Observer<IPerson[]>) => {
      observer.next([
        {
          'name': 'Eugene',
          'surname': 'Korablev'
        },
        {
          "name": "Eugene 2",
          "surname": "Korablev 2"
        }
      ]);
      observer.complete();
    });
  }
}
