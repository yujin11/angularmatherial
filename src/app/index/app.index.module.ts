import {NgModule} from '@angular/core';
import {IndexComponent} from './component/app.index.component';
import {
  MatButtonModule,
  MatCardModule,
  MatInputModule,
  MatOptionModule,
  MatSelectModule
} from "@angular/material";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {AppServices} from "../services/app.services";
import {StoreModule} from "@ngrx/store";
import {BrowserModule} from "@angular/platform-browser";
import {ShareModule} from "../app.share.module";
import {BrowserAnimationsModule} from "../../../node_modules/@angular/platform-browser/animations";
import {PersonsModule} from "../persons/persons.module";
import {reducer} from "./store/reducers";
import {RouterModule} from "@angular/router";
import {CustomFormsModule} from "../forms/forms.module";
import {ChildModule} from "../child/child.module";

@NgModule({
  declarations: [
    IndexComponent,
  ],
  imports: [
    BrowserModule,
    ShareModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    PersonsModule,
    ChildModule,
    StoreModule.forFeature('counter', reducer),
    RouterModule,
    MatCardModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatOptionModule,
    CustomFormsModule
  ],
  providers: [AppServices],
  exports: [
    IndexComponent
  ]
})
export class AppIndexModule {
}
