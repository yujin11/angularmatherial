import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, EventEmitter,
} from '@angular/core';
import {Observable} from "rxjs";
import {Store} from "@ngrx/store";
import {AppServices} from "../../services/app.services";
import * as fromCounter from "../store/index";

interface CounterStore {
  counter: number;
}

declare let numeral: any;

@Component({
  selector: 'app-index',
  templateUrl: './app.index.component.html',
  styleUrls: ['./app.index.component.less']
})
export class IndexComponent {
  public inputVal = 'Value';

  counter$: Observable<number>;

  number: number = 5;
  numberStr: string;
  format: string = '0.00';

  expNumbers: number = 0;

  expNumbersChangeEvent(event){
    this.expNumbers = event;
  }

  constructor(private store: Store<CounterStore>, private appService: AppServices, protected cd: ChangeDetectorRef) {
    this.counter$ = store.select<any>(fromCounter.getCounter);
    this.counter$.subscribe((data: number) => {
      console.log('Counter: ', data);
    })

  }

  title = 'app';

  increace() {
    this.store.dispatch(new fromCounter.CounterIncrease());
  }

  decreace() {
    this.store.dispatch(new fromCounter.CounterDecrease());
  }

  reset() {
    this.store.dispatch(new fromCounter.CounterReset(0));
  }

  updateFormat(event: any) {
    this.format = event.target.value;
    this.cd.markForCheck();
  }
  updateInput(event: any) {
    this.number = event.target.value;
    // this.numberStr = numeral(this.number).format();
    this.cd.markForCheck();
  }
}
