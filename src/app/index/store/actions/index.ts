import {CounterDecrease, CounterIncrease, CounterReset} from "./counter.actions";

export type counterActions = CounterIncrease | CounterDecrease | CounterReset;
export * from "./counter.actions";
