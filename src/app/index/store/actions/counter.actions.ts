import {Action} from "@ngrx/store";

export const COUNTER_INCREASE = '[Counter] Inctrease';
export const COUNTER_DECREASE = '[Counter] Decrease';
export const COUNTER_RESET = '[Counter] Reset';

export class CounterIncrease implements Action {
  readonly type: string = COUNTER_INCREASE;
}

export class CounterDecrease implements Action {
  readonly type: string = COUNTER_DECREASE;
}

export class CounterReset implements Action {
  readonly type: string = COUNTER_RESET;
  constructor(public payload: number) {
  }
}
