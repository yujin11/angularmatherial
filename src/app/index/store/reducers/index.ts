import * as fromReducer from "./counter.reducers";
import {createFeatureSelector, createSelector, ActionReducerMap} from "@ngrx/store";

export interface CounterState {
  counter: fromReducer.CounterState;
}

export const reducer: ActionReducerMap<CounterState> = {
  counter: fromReducer.counterReducer
}

export const getCountersState = createFeatureSelector('counter');

export const getCounterState = createSelector(getCountersState, (state: CounterState) => {
  return state.counter;
});
export const getCounter = createSelector(getCounterState, fromReducer.getCounter);

