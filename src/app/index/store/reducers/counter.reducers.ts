import * as fromCounterAction from "../actions/index";

export interface CounterState {
  counter: number;
}

export const initialState: CounterState = {
  counter: 10
}

export function counterReducer(state = initialState, action: fromCounterAction.counterActions): CounterState {
  switch (action.type) {
    case fromCounterAction.COUNTER_INCREASE:
      return {
        ...state, counter: state.counter + 1
      };
    case fromCounterAction.COUNTER_DECREASE:
      return {
        ...state, counter: state.counter - 1
      };
    case fromCounterAction.COUNTER_RESET:
      return {
        ...state, counter: 0
      };

  }
  return state;
}

export const getCounter = (state: CounterState) => {
  return state.counter;
}
