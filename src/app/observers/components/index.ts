import {ObservableComponent} from "./observable.component";

export {ObservableComponent};

export const observableComponents = [ObservableComponent];
