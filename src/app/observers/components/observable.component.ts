import {Component, OnInit} from "@angular/core";
import {range, interval, fromEvent, merge} from "rxjs";
import {take, switchMap, mergeMap, mapTo, map, combineAll} from "rxjs/operators";

@Component({
  selector: 'app-observable',
  templateUrl: './observable.component.html'
})
export class ObservableComponent implements OnInit {
  data$ = range(1, 10);
  interval$ = interval(100);
  clicks$ = fromEvent(document, 'click');
  innerObservable$ = interval(1000);
  first$ = interval(2500);
  second$ = interval(1500);
  third$ = interval(2000);
  fourth$ = interval(3000);
  m = {id: 5, message: 'message'};

  ngOnInit(): void {
    this.data$.pipe(take(5)).subscribe(val => console.log(`Take 2: ${val}`));
    // this.interval$.subscribe(val=> console.log("Interval: ", val));
    this.clicks$.pipe(mergeMap(event => {
      console.log('Event: ', event);
      return this.innerObservable$
    })).subscribe(val => console.log(val));
    // this.innerObservable$.pipe(mapTo(this.m)).subscribe(d=> console.log(d.message));

    // merge(this.first$.pipe(mapTo('First')),
    //   this.second$.pipe(mapTo('Second')),
    //   this.third$.pipe(mapTo('Third')),
    //   this.fourth$.pipe(mapTo('Fourth'))
    // ).subscribe(res=>console.log(res));

    this.innerObservable$.pipe(take(10)).pipe(val=> interval(1000).pipe(map(i=> 'Result (${val}) : ${i}'), take(5))).pipe(combineAll());
  }
}
