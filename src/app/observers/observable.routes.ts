import {Routes} from "@angular/router";
import {ObservableComponent} from "./components/observable.component";

export const routes: Routes = [
  {
    path: 'observable',
    component: ObservableComponent
  }
]
