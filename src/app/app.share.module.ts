import {NgModule, Optional, SkipSelf} from "@angular/core";
import {MatButtonModule, MatCheckboxModule} from "@angular/material";
import 'hammerjs';

@NgModule({
  imports:[
    MatCheckboxModule,
    MatButtonModule
  ],
  exports:[
    MatCheckboxModule,
    MatButtonModule
  ]
})
export class ShareModule {
  constructor (
    @Optional()
    @SkipSelf()
      parentModule: ShareModule
  ) {
    if (parentModule) {
      throw new Error('ShareModule is already loaded. Import only in AppModule');
    }
  }
}
