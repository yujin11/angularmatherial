import {Routes} from "@angular/router";
import {AppIndexModule} from "../index/app.index.module";
import {IndexComponent} from "../index/component/app.index.component";

export const routes: Routes = [
  {
    path: '',
    component: IndexComponent
  }
]
